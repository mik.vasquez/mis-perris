from .models import Adopcion
from django.shortcuts import render,redirect
from .forms import AdopcionForm, PostForm
from django.contrib.auth import login, logout, authenticate
from django.contrib.auth.forms import UserCreationForm, AuthenticationForm
from django.contrib.auth.models import User
from.forms import RegisterForm


# Create your views here.
def formulario(request):
    if request.method == "POST":
        form = PostForm(request.POST)
        if form.is_valid():
            form.save()
            return redirect('form')
    else:
        form = PostForm()
    return render(request, 'index.html',{'form': form})

    
def main(request):
    return render(request, 'Mis Perris.html')    
 
def servicios(request):
    if request.method == "POST":
        form = AdopcionForm(request.POST, request.FILES)
        if form.is_valid():
            form.save()
            return redirect('servicios')
    else:
        form = AdopcionForm()
    return render(request,'Servicios.html',{'form': form})

#VARIBLES DEL LOGIN Y OUT
def registro(request):
    if(request.method == 'POST'):
        form = RegisterForm(request.POST)
        if form.is_valid():
            form.save()
        return redirect('mis_perris')
    else:
        form = RegisterForm()
    return render(request, 'registro.html',{'form':form})

def ingreso(request):
    if(request.method == 'POST'):
        form = AuthenticationForm(data=request.POST)
        if(form.is_valid()):
            user = authenticate(request, username=request.POST['username'], password=request.POST['password'])
            if(user is not None):
                login(request, user)
                return redirect('mis_perris')
    else:
        form = AuthenticationForm
    return render(request, 'ingreso.html',{'form':form})

def out(request):
    logout(request)
    return redirect('mis_perris')
#VARIABLE PARA LISTAR LOS PERROS
def perro_list(request):
    adopcion = Adopcion.objects.all()
    return render(request, 'listar_perros.html',{'adopcion': adopcion})


#def form_list(request):
 #   form = AdopcionForm.objects.all()
  #  return render(request,'Servicios.html',{'form': form})

#def form_detalle(request, pk):
 #   form = get_object_or_404(AdopcionForm, pk=pk)
   # return render(request, 'Servicios.html', {'form':form})

