var imagenes = ['static/misperris/img/img0.png','static/misperris/img/img1.jpg','static/misperris/img/img2.jpg','static/misperris/img/img3.jpg','static/misperris/img/img4.jpg','static/misperris/img/img5.jpg','static/misperris/img/img6.jpg','static/misperris/img7.jpg'],
    cont = 0;

function carrousel(contenedor){
    contenedor.addEventListener('click', e => {
        let atras = contenedor.querySelector('.atras'),
            adelante = contenedor.querySelector('.adelante'),
            img = contenedor.querySelector('img'),
            tgt = e.target;

            if(tgt == atras){
                if(cont > 0){
                    img.src = imagenes[cont - 1];
                    cont--;
                } else {
                    img.src = imagenes[imagenes.length - 1];
                    cont = imagenes.length - 1;
                }

            } else if(tgt == adelante){
                if(cont < imagenes.length - 1){
                    img.src = imagenes[cont + 1];
                    cont++;
                } else {
                    img.src = imagenes[0];
                    cont = 0;
                }
            }
    });
}

document.addEventListener("DOMContentLoaded", () => {
    let contenedor = document.querySelector('.img');

    carrousel(contenedor);
});