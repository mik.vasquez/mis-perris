from django.db import models

# Create your models here.
# CUANDO SE CAMBIA ALGO HACER EL MIGRATE

OPCIONES_ESTADO = (
    ('1', 'Rescatado'),
    ('2', 'Disponible'),
    ('3', 'Adoptado'),
)

class Adopcion(models.Model):
    Nombre = models.CharField(max_length=100)
    Raza   = models.CharField(max_length=100)
    Descripcion = models.CharField(max_length=100)
    Foto = models.ImageField(upload_to='media')
    Estado = models.CharField(choices=OPCIONES_ESTADO, max_length=10,default=1)

    def __str__(self):
        return'{} {} {} {}'.format(self.Nombre,self.Raza, self.Descripcion, str(self.Estado))



class User(models.Model):
    Email = models.EmailField(max_length=100)
    Rut = models.CharField(max_length=12)
    Nombre1 = models.CharField(max_length=100)
    FechaNacimiento = models.DateField()
    Telefono = models.CharField(max_length=9)
    Usuario = models.CharField(max_length=100)
    Clave = models.CharField(max_length=100)
    
    def publish(self): 
        self.save()

    def __str__(self):
        return'{} {} {} {} {} {} {}'.format(self.Email,self.Rut,self.Nombre1, str(self.FechaNacimiento), self.Telefono,
        self.Usuario,self.Clave)
