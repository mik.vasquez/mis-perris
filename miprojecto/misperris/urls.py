from django.urls import path, re_path
from . import views
from django.conf import settings
from django.conf.urls.static import static

urlpatterns=[
    re_path(r'^$', views.main,name="mis_perris"),

    path('formulario', views.formulario,name="form"),

    path('servicios', views.servicios,name="servicios"),

    path('registro', views.registro, name='registro'),
    
    path('ingreso', views.ingreso, name='ingreso'),

    path('out', views.out, name='out'),

    path('list', views.perro_list, name="listar"),
  
]

