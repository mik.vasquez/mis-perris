from django import forms 
from .models import Adopcion, User
from django.contrib.auth.forms import UserCreationForm  
from django.contrib.auth.models import User


class AdopcionForm(forms.ModelForm):
    Nombre = forms.CharField(widget=forms.TextInput(
        attrs={
            'class': 'form-control',
        }
    ))

    Raza = forms.CharField(widget=forms.TextInput(
        attrs={
            'class': 'form-control',
        }
    ))   
    
    Descripcion = forms.CharField(widget=forms.Textarea(
        attrs={
            'class': 'form-control',
        }
    ))     

    Nombre = forms.CharField(widget=forms.TextInput(
        attrs={
            'class': 'form-control',
        }
    ))   
    
     
    class Meta:
        model = Adopcion       
        fields = ('Nombre', 'Raza', 'Descripcion', 'Foto', 'Estado', )

#FORMULARIO DEL USUARIO

class PostForm(forms.ModelForm):

    Email = forms.CharField(widget=forms.TextInput(
        attrs={
            'class': 'form-control',
        }
    ))
    Rut = forms.CharField(widget=forms.TextInput(
        attrs={
            'class': 'form-control',
        }
    ))

    Nombre1 = forms.CharField(widget=forms.TextInput(
        attrs={
            'class': 'form-control',
        }
    ))

    FechaNacimiento= forms.CharField(widget=forms.TextInput(
        attrs={
            'class': 'form-control',
        }
    ))

    Telefono = forms.CharField(widget=forms.TextInput(
        attrs={
            'class': 'form-control',
        }
    ))

    Usuario = forms.CharField(widget=forms.TextInput(
        attrs={
            'class': 'form-control',
        }
    ))

    Clave = forms.CharField(widget=forms.TextInput(
        attrs={
            'class': 'form-control',
        }
    ))

  
    class Meta:
        model = User       
        fields = ('Email', 'Rut', 'Nombre1', 'FechaNacimiento', 'Telefono', 'Usuario', 'Clave', )
       # <!--labels = {
            
        #} 

class RegisterForm(UserCreationForm):
    email = forms.EmailField(required=True)
 
    class Meta:
        model = User
        fields = ("first_name", 
                 "last_name", 
                 "email", 
                 "username", 
                 "password1", 
                 "password2", )
 
  
    def save(self, commit=True):
        user = super(RegisterForm, self).save(commit=False)
        user.email = self.cleaned_data["email"]
        user.first_name = self.cleaned_data["first_name"]
        user.last_name = self.cleaned_data["last_name"]
 
        if commit:
            user.save()

        return user